package com.company.csc.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "CSC_COMPANY")
@Entity(name = "csc_Company")
public class Company extends StandardEntity {
    private static final long serialVersionUID = -4918124793415902494L;

    @NotNull
    @Column(name = "INN", nullable = false)
    protected String inn;

    @Lookup(type = LookupType.DROPDOWN)
    @NotNull
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CUSTOMER_ID")
    protected Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }
}
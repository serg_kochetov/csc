package com.company.csc.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "CSC_INDIVIDUAL")
@Entity(name = "csc_Individual")
public class Individual extends StandardEntity {
    private static final long serialVersionUID = -7960077245615790170L;

    @NotNull
    @Column(name = "PASSPORT_NO", nullable = false)
    protected String passportNo;

    @Lookup(type = LookupType.DROPDOWN)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    protected Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }
}
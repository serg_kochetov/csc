package com.company.csc.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@NamePattern("%s|firstName")
@Table(name = "CSC_EMPLOYEE")
@Entity(name = "csc_Employee")
public class Employee extends StandardEntity {
    private static final long serialVersionUID = 3933316915541067413L;

    @NotNull
    @Column(name = "FIRST_NAME", nullable = false, length = 50)
    protected String firstName;

    @NotNull
    @Column(name = "LAST_NAME", nullable = false, length = 50)
    protected String lastName;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "BIRTH_DATE", nullable = false)
    protected Date birthDate;

    @Email(message = "Email is not valid")
    @NotNull
    @Column(name = "EMAIL", nullable = false, length = 50)
    protected String email;

    @NotNull
    @Column(name = "SALARY", nullable = false)
    protected BigDecimal salary;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CENTER_ID")
    protected CarServiceCenter center;

    public CarServiceCenter getCenter() {
        return center;
    }

    public void setCenter(CarServiceCenter center) {
        this.center = center;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
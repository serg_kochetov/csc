package com.company.csc.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Table(name = "CSC_CUSTOMER")
@Entity(name = "csc_Customer")
@NamePattern("%s|name")
public class Customer extends StandardEntity {
    private static final long serialVersionUID = 9131024147839773097L;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 50)
    protected String name;

    @NotNull
    @Column(name = "PHONE", nullable = false, length = 50)
    protected String phone;

    @Email(message = "Email is not valid")
    @NotNull
    @Column(name = "EMAIL", nullable = false, length = 50)
    protected String email;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "customer")
    protected Individual individual;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "customer")
    protected Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
package com.company.csc.web.screens.individual;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.Individual;

@UiController("csc_Individual.edit")
@UiDescriptor("individual-edit.xml")
@EditedEntityContainer("individualDc")
@LoadDataBeforeShow
public class IndividualEdit extends StandardEditor<Individual> {
}
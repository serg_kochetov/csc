package com.company.csc.web.screens.company;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.Company;

@UiController("csc_Company.edit")
@UiDescriptor("company-edit.xml")
@EditedEntityContainer("companyDc")
@LoadDataBeforeShow
public class CompanyEdit extends StandardEditor<Company> {
}
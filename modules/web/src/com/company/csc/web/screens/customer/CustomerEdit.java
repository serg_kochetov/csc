package com.company.csc.web.screens.customer;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.Customer;

@UiController("csc_Customer.edit")
@UiDescriptor("customer-edit.xml")
@EditedEntityContainer("customerDc")
@LoadDataBeforeShow
public class CustomerEdit extends StandardEditor<Customer> {
}
package com.company.csc.web.screens.individual;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.Individual;

@UiController("csc_Individual.browse")
@UiDescriptor("individual-browse.xml")
@LookupComponent("individualsTable")
@LoadDataBeforeShow
public class IndividualBrowse extends StandardLookup<Individual> {
}
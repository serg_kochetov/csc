package com.company.csc.web.screens.company;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.Company;

@UiController("csc_Company.browse")
@UiDescriptor("company-browse.xml")
@LookupComponent("companiesTable")
@LoadDataBeforeShow
public class CompanyBrowse extends StandardLookup<Company> {
}
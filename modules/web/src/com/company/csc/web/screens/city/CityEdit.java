package com.company.csc.web.screens.city;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.City;

@UiController("csc_City.edit")
@UiDescriptor("city-edit.xml")
@EditedEntityContainer("cityDc")
@LoadDataBeforeShow
public class CityEdit extends StandardEditor<City> {
}
package com.company.csc.web.screens.customer;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.Customer;

@UiController("csc_Customer.browse")
@UiDescriptor("customer-browse.xml")
@LookupComponent("customersTable")
@LoadDataBeforeShow
public class CustomerBrowse extends StandardLookup<Customer> {
}
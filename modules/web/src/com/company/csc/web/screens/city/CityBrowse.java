package com.company.csc.web.screens.city;

import com.haulmont.cuba.gui.screen.*;
import com.company.csc.entity.City;

@UiController("csc_City.browse")
@UiDescriptor("city-browse.xml")
@LookupComponent("citiesTable")
@LoadDataBeforeShow
public class CityBrowse extends StandardLookup<City> {
}